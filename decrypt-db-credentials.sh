#!/bin/bash

aws kms decrypt \
  --ciphertext-blob fileb://terraform/modules/rds/db-creds.yml.encrypted \
  --query Plaintext \
  --output text | base64 \
  --decode > db-creds.yml