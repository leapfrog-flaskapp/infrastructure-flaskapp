#!/bin/bash

cms_key_id=$1

if [[ $cms_key_id == "" ]];then
   echo "no kms key id passed for encryption. Pass in the key id as argument"
   echo "command format = ./encrypt-db-credentials.sh your_aws_kms_key_id"
   exit 1
fi



FILE="db-creds.yml"
if test -f "$FILE"; then
    echo "encrypting credentials."
    aws kms encrypt \
      --key-id $cms_key_id \
      --plaintext fileb://db-creds.yml \
      --output text \
      --query CiphertextBlob | base64 \
      --decode > /tmp/encrypted-db-cred.tmp

      if [ -f /tmp/encrypted-db-cred.tmp  ]
      then
        if [ -s /tmp/encrypted-db-cred.tmp  ]
        then
            cat /tmp/encrypted-db-cred.tmp > terraform/modules/rds/db-creds.yml.encrypted
        else
            echo "encryption failed. Check your key id"
        fi
       else
        echo "encryption failed."
      fi  

else
    echo -e "ERROR!! No file db-creds.yml found in current directroy.\nCreate one and place your database username and password there like following.\nusername: someuser\npassword: somepassword"
fi