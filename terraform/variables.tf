variable "projectname" {
  default = "flaskapp"
  type    = string
}

variable "region" {
  default = "ap-southeast-2"
  type    = string
}

variable "aws_credential_path" {
  default = "~/.aws/credentials"
  type    = string
}

variable "aws_credential_profile" {
  default = "default"
  type    = string
}

