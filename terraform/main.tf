# ##manual deployment
#  provider "aws" {
#    region                  = var.region
#    shared_credentials_file = var.aws_credential_path
#    profile                 = var.aws_credential_profile
#  }
##manual deployment

#version control
provider "aws" {}
terraform {
 backend "http" {
 }
}
#version control

module "vpc" {
  source = "./modules/vpc/"
}

module "rds" {
  source                 = "./modules/rds/"
  vpc                    = module.vpc.vpc
  database_subnet_az1    = module.vpc.database_subnet_az1
  database_subnet_az2    = module.vpc.database_subnet_az2
  database_subnet_az3    = module.vpc.database_subnet_az3
  database_securitygroup = module.vpc.database_securitygroup
  db_name                = var.projectname
  snaphot_update_count   = 1
  last_snapshot          = false
  db_instance_type       = "db.t2.micro"
}

module "iam" {
  source = "./modules/iam/"
}

module "s3" {
  source          = "./modules/s3/"
  buketnameprefix = var.projectname
}

module "elb" {
  source = "./modules/elb"

  alb_securitygroup = module.vpc.alb_securitygroup
  alb_subnet_az1    = module.vpc.alb_subnet_az1
  alb_subnet_az2    = module.vpc.alb_subnet_az2
  alb_subnet_az3    = module.vpc.alb_subnet_az3
  vpc               = module.vpc.vpc
  access_log_bucket = module.s3.alb_log_bucket
  alb_name          = var.projectname
}

module "cloudwatch_logs" {
  source         = "./modules/cloudwatch_logs"
  log_group_name = var.projectname
}

module "ecs" {
  source                = "./modules/ecs/"
  fargate_ecs_role      = module.iam.fargate_ecs_role
  fargate_subnet_az1    = module.vpc.fargate_subnet_az1
  fargate_subnet_az2    = module.vpc.fargate_subnet_az2
  fargate_subnet_az3    = module.vpc.fargate_subnet_az3
  fargate_securitygroup = module.vpc.fargate_securitygroup
  fargate_targetgroup   = module.elb.fargate_alb_targetgroup
  task_cpu              = 256
  task_memory           = 512
  container_image       = "registry.gitlab.com/flaskapp-leapfrog/flaskapp:latest"
  container_name        = var.projectname
  task_family           = var.projectname
  cloudwatch_log_group  = module.cloudwatch_logs.access_log_loggroup
}

module "autoscaling" {
  source                  = "./modules/autoscale/"
  fargate_cluster         = module.ecs.fargate_cluster
  fargate_cluster_service = module.ecs.fargate_cluster_service
  scale_out_cooldown      = 20
  scale_in_cooldown       = 20
  scale_min_capacity      = 1
  scale_max_capacity      = 2
  average_memory_usage    = 11
}

module "acm" {
  source = "./modules/acm/"
  # server_certificate_path = "${path.cwd}/../vpncertificates/server_certificate_body.crt"
  # server_key_path = "${path.cwd}/../vpncertificates/server_key.key"
  # client_certificate_path = "${path.cwd}/../vpncertificates/client_certificate_body.crt"
  # client_key_path = "${path.cwd}/../vpncertificates/client_key.key"
  # ca_path = "${path.cwd}/../vpncertificates/ca.crt"
}

module "clientvpn" {
  source                 = "./modules/vpn/"
  loggroup               = module.cloudwatch_logs.clientvpn_log_loggroup
  vpn_client_certificate = module.acm.clientvpn_client_certificate
  vpn_server_certificate = module.acm.clientvpn_server_certificate
  database_subnet_az1    = module.vpc.database_subnet_az1
  database_subnet_az2    = module.vpc.database_subnet_az2
  database_subnet_az3    = module.vpc.database_subnet_az3
  vpn_securitygroup = module.vpc.vpn_securitygroup
}

output "elb_DNS_NAME" {
  value = module.elb.elb_endpoint
}

output "database_endpoint" {
  value = module.rds.db_endpoint
}
