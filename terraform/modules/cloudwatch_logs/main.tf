resource "aws_cloudwatch_log_group" "fargate-logs" {
  name = "${var.log_group_name}-fargate-logs"

  tags = {
    Name = "flaskapp"
  }
}

resource "aws_cloudwatch_log_group" "clientvpn-logs" {
  name = "${var.log_group_name}-clientvpn-logs"

  tags = {
    Name = "flaskapp"
  }
}

