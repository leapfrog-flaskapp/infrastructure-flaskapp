output "access_log_loggroup"{
    value = aws_cloudwatch_log_group.fargate-logs
}

output "clientvpn_log_loggroup"{
    value = aws_cloudwatch_log_group.clientvpn-logs
}