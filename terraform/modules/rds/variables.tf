variable "db_instance_type" {
    type = string
}

variable "last_snapshot"{
    type = bool
}

variable "snaphot_update_count"{
    type = number
}

variable "db_name"{
    type = string
}

variable "database_subnet_az1"{}
variable "database_subnet_az2"{}
variable "database_subnet_az3"{}
variable "database_securitygroup"{}
variable "vpc"{}