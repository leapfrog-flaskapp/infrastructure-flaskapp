data "aws_kms_secrets" "dbsecret" {
  secret {
    name    = "database"
    payload = filebase64("${path.module}/db-creds.yml.encrypted")
  }
}
locals {
  db_creds = yamldecode(data.aws_kms_secrets.dbsecret.plaintext["database"])
}

#change the keeprs value if you want to dchange the snapshots backupname
resource "random_string" "snapshotrandomname" {
  length           = 10
  special          = false
  keepers           = {
    change_name_count  = var.snaphot_update_count
  }
}

#parameter group for flaskappdb
resource "aws_db_parameter_group" "flaskapp" {
  name = "flaskapp"
  family= "mysql8.0"
}

#subnet group for flaskapp db
resource "aws_db_subnet_group" "flaskappdbsg" {
  name = "flaskappag"
  subnet_ids = [var.database_subnet_az1.id, var.database_subnet_az2.id, var.database_subnet_az3.id]

  tags = {
    Name = "flaskappDB subnet group"
  }
}

#RDS flaskapp db
resource "aws_db_instance" "flaskappdb" {
  name = var.db_name
  identifier = var.db_name
  allocated_storage = 20
  storage_type = "gp2"
  engine = "mysql"
  engine_version = "8.0"
  instance_class = var.db_instance_type
  parameter_group_name = aws_db_parameter_group.flaskapp.name
  db_subnet_group_name = aws_db_subnet_group.flaskappdbsg.name
  vpc_security_group_ids = [var.database_securitygroup.id]
  skip_final_snapshot  = var.last_snapshot
  final_snapshot_identifier = "${var.db_name}-final-${random_string.snapshotrandomname.result}"
  username = local.db_creds.username
  password = local.db_creds.password
}