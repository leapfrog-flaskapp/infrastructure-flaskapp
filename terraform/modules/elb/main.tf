resource "aws_lb" "flaskapp-lb" {
  name               = var.alb_name
  internal           = false
  load_balancer_type = "application"
  security_groups    = [var.alb_securitygroup.id]
  subnets            = [var.alb_subnet_az1.id,var.alb_subnet_az2.id,var.alb_subnet_az3.id]

  access_logs {
    bucket  = var.access_log_bucket.bucket
    enabled = true
  }

  tags = {
    Name = "${var.alb_name}"
  }
}

resource "aws_lb_target_group" "flaskapp-fargate-tg" {
  name     = "flaskapp-fargate-tg"
  port     = 80
  protocol = "HTTP"
  target_type = "ip"
  vpc_id   = var.vpc.id
  deregistration_delay = 30
}

resource "aws_lb_listener" "flaskapp-lb-listener" {
  load_balancer_arn = aws_lb.flaskapp-lb.arn
  port              = "80"
  protocol          = "HTTP"
 
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.flaskapp-fargate-tg.arn
  }
}