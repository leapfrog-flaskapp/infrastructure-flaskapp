output "elb_endpoint" {
    value = aws_lb.flaskapp-lb.dns_name
}

output "fargate_alb_targetgroup" {
  value = aws_lb_target_group.flaskapp-fargate-tg
}