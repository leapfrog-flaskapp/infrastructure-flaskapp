variable "alb_securitygroup"{}
variable "alb_subnet_az1" {}
variable "alb_subnet_az2" {}
variable "alb_subnet_az3" {}
variable "vpc" {}
variable "access_log_bucket"{}

variable "alb_name" {
  type = string
}