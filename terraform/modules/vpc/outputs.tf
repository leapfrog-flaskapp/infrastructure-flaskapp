output "vpc" {
    value = aws_vpc.vpc
}

output "alb_subnet_az1"{
    value = aws_subnet.alb-az1
}

output "alb_subnet_az2"{
    value = aws_subnet.alb-az2
}

output "alb_subnet_az3"{
    value = aws_subnet.alb-az3
}

output "fargate_subnet_az1"{
    value = aws_subnet.fargate-az1
}

output "fargate_subnet_az2"{
    value = aws_subnet.fargate-az2
}

output "fargate_subnet_az3"{
    value = aws_subnet.fargate-az3
}

output "database_subnet_az1"{
    value = aws_subnet.database-az1
}

output "database_subnet_az2"{
    value = aws_subnet.database-az2
}

output "database_subnet_az3"{
    value = aws_subnet.database-az3
}

output "alb_securitygroup" {
    value = aws_security_group.SG_ELB_FARGATE
}

output "fargate_securitygroup" {
    value = aws_security_group.SG_FARGATE
}

output "database_securitygroup" {
    value = aws_security_group.SG_DATABASE
}

output "vpn_securitygroup" {
    value = aws_security_group.SG_VPNCLIENT
}
