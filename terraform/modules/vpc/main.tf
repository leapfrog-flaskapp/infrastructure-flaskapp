data "aws_availability_zones" "zone" {}

resource "aws_vpc" "vpc" {
  cidr_block = "192.168.0.0/16"
  tags={
    Name = "flaskapp-vpc"
  }
}


resource "aws_subnet" "fargate-az1" {
  vpc_id = aws_vpc.vpc.id
  availability_zone = data.aws_availability_zones.zone.names[0]
  cidr_block = "192.168.1.0/24"
  map_public_ip_on_launch = false
  tags = {
    "Name" = "fargate-az1"
  }
}

resource "aws_subnet" "fargate-az2" {
  vpc_id = aws_vpc.vpc.id
  availability_zone = data.aws_availability_zones.zone.names[1]
  cidr_block = "192.168.2.0/24"
  map_public_ip_on_launch = false
  tags = {
    "Name" = "fargate-az2"
  }
}

resource "aws_subnet" "fargate-az3" {
  vpc_id = aws_vpc.vpc.id
  availability_zone = data.aws_availability_zones.zone.names[2]
  cidr_block = "192.168.3.0/24"
  map_public_ip_on_launch = false
  tags = {
    "Name" = "fargate-az3"
  }
}

#subnet for rds in availability zone 1
resource "aws_subnet" "database-az1" {
  vpc_id = aws_vpc.vpc.id
  availability_zone = data.aws_availability_zones.zone.names[0]
  cidr_block = "192.168.4.0/24"
  map_public_ip_on_launch = false
  tags = {
    "Name" = "database-az1"
  }
}

#subnet for rds in availability zone 2
resource "aws_subnet" "database-az2" {
  vpc_id = aws_vpc.vpc.id
  availability_zone = data.aws_availability_zones.zone.names[1]
  cidr_block = "192.168.5.0/24"
  map_public_ip_on_launch = false
  tags = {
    "Name" = "database-az2"
  }
}

#subnet for rds in availability zone 3
resource "aws_subnet" "database-az3" {
  vpc_id = aws_vpc.vpc.id
  availability_zone = data.aws_availability_zones.zone.names[2]
  cidr_block = "192.168.6.0/24"
  map_public_ip_on_launch = false
  tags = {
    "Name" = "database-az3"
  }
}

#subnet for public network zone 1
resource "aws_subnet" "alb-az1" {
  vpc_id = aws_vpc.vpc.id
  availability_zone = data.aws_availability_zones.zone.names[0]
  cidr_block = "192.168.7.0/24"
  map_public_ip_on_launch = false
  tags = {
    "Name" = "alb-az1"
  }
}

#subnet for public network zone 2
resource "aws_subnet" "alb-az2" {
  vpc_id = aws_vpc.vpc.id
  availability_zone = data.aws_availability_zones.zone.names[1]
  cidr_block = "192.168.8.0/24"
  map_public_ip_on_launch = false
  tags = {
    "Name" = "alb-az2"
  }
}

#subnet for public network zone 3
resource "aws_subnet" "alb-az3" {
  vpc_id = aws_vpc.vpc.id
  availability_zone = data.aws_availability_zones.zone.names[2]
  cidr_block = "192.168.9.0/24"
  map_public_ip_on_launch = false
  tags = {
    "Name" = "alb-az3"
  }
}

#flaskapp_internet_gateway for public networks
resource "aws_internet_gateway" "alb-internet-gateway" {
 vpc_id = aws_vpc.vpc.id
 tags = {
   "Name" = "fargate-ig"
 }
}

#flaskapp EIP for nat gateway
resource "aws_eip" "fargate-ig-eip" {
  vpc=true
}

#flaskapp_natgateway for private networks
resource "aws_nat_gateway" "fargate-nat-gw" {
  allocation_id = aws_eip.fargate-ig-eip.id
  subnet_id     = aws_subnet.alb-az1.id

  tags = {
    Name = "fargate-nat-gw"
  }

  depends_on = [aws_internet_gateway.alb-internet-gateway]
}


#flaskapp_routetable_public
resource "aws_route_table" "public-routetable" {
  vpc_id = aws_vpc.vpc.id
  route = [ {
    cidr_block = "0.0.0.0/0"
    egress_only_gateway_id = null
    gateway_id = aws_internet_gateway.alb-internet-gateway.id
    instance_id = null
    ipv6_cidr_block = null
    local_gateway_id = null
    nat_gateway_id = null
    network_interface_id = null
    transit_gateway_id = null
    vpc_endpoint_id = null
    vpc_peering_connection_id = null
    carrier_gateway_id = null
    destination_prefix_list_id = null
  } ]

  tags = {
    "Name" = "public-routetable"
  }
}

#flaskapp_routetable_private
resource "aws_route_table" "private-routetable" {
  vpc_id = aws_vpc.vpc.id

   route = [ {
    cidr_block = "0.0.0.0/0"
    egress_only_gateway_id = null
    gateway_id = null
    instance_id = null
    ipv6_cidr_block = null
    local_gateway_id = null
    nat_gateway_id = aws_nat_gateway.fargate-nat-gw.id
    network_interface_id = null
    transit_gateway_id = null
    vpc_endpoint_id = null
    vpc_peering_connection_id = null
    carrier_gateway_id = null
    destination_prefix_list_id = null
  } ]

  tags = {
    "Name" = "private-routetable"
  }
}

########FLASKAPP FARGATE######################
resource "aws_route_table_association" "fargate-az1" {
  subnet_id = aws_subnet.fargate-az1.id
  route_table_id = aws_route_table.private-routetable.id
}

resource "aws_route_table_association" "fargate-az2" {
  subnet_id = aws_subnet.fargate-az2.id
  route_table_id = aws_route_table.private-routetable.id
}

resource "aws_route_table_association" "fargate-az3" {
  subnet_id = aws_subnet.fargate-az3.id
  route_table_id = aws_route_table.private-routetable.id
}

######flaskapp_routetable_association RDS############################
resource "aws_route_table_association" "database-az1" {
  subnet_id = aws_subnet.database-az1.id
  route_table_id = aws_route_table.private-routetable.id
}

resource "aws_route_table_association" "database-az2" {
  subnet_id = aws_subnet.database-az2.id
  route_table_id = aws_route_table.private-routetable.id
}

resource "aws_route_table_association" "database-az3" {
  subnet_id = aws_subnet.database-az3.id
  route_table_id = aws_route_table.private-routetable.id
}

######flaskapp_routetable_association public############################
resource "aws_route_table_association" "alb-az1" {
  subnet_id = aws_subnet.alb-az1.id
  route_table_id = aws_route_table.public-routetable.id
}

resource "aws_route_table_association" "alb-az2" {
  subnet_id = aws_subnet.alb-az2.id
  route_table_id = aws_route_table.public-routetable.id
}

resource "aws_route_table_association" "alb-az3" {
  subnet_id = aws_subnet.alb-az3.id
  route_table_id = aws_route_table.public-routetable.id
}

#FARGATE SECUTIRY GROUP SG_ELB_FARGATE
resource "aws_security_group" "SG_ELB_FARGATE" {
  name        = "SG_ELB_FARGATE"
  description = "inbound traffic for ELB for fargate"
  vpc_id      = aws_vpc.vpc.id

  egress {
    description = "Outgoing raffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description = "HTTPS connection"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
  
  ingress {
    description = "HTTP connection"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "SG_ELB_FARGATE"
  }
}

#FARGATE SECUTIRY GROUP SG_FARGATE
resource "aws_security_group" "SG_FARGATE" {
  name        = "SG_FARGATE"
  description = "inbound traffic for flaskapp fargate instances"
  vpc_id      = aws_vpc.vpc.id

  egress {
    description = "Outgoing raffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description = "HTTPS connection"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    security_groups = [aws_security_group.SG_ELB_FARGATE.id]
  }
  
  ingress {
    description = "HTTP connection"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    security_groups = [aws_security_group.SG_ELB_FARGATE.id]
  }

  ingress {
    description = "HTTP connection"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }


  tags = {
    Name = "SG_FARGATE"
  }
}

#FARGATE SECUTIRY GROUP SG_VPNCLIENT
resource "aws_security_group" "SG_VPNCLIENT" {
  name        = "SG_VPNCLIENT"
  description = "traffic for vpn clients"
  vpc_id      = aws_vpc.vpc.id

  egress {
    description = "Outgoing raffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description = "HTTPS connection"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "SG_VPNCLIENT"
  }
}


#RDS SECURITY GROUP SG_DATABASE
resource "aws_security_group" "SG_DATABASE" {
  name        = "SG_DATABASE"
  description = "traffic for flaskapp RDS instances"
  vpc_id      = aws_vpc.vpc.id

  egress {
    description = "Outgoing raffic"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description = "SG_FARGATE MYSQL CONNECTION"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    security_groups = [aws_security_group.SG_FARGATE.id]
  }

  ingress {
    description = "SG_CLIENTVPN MYSQL CONNECTION"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    security_groups = [aws_security_group.SG_VPNCLIENT.id]
  }

  tags = {
    Name = "SG_DATABASE"
  }
}