#create a iam execution role for our task to fetch containers and access cloudwatch logs
resource "aws_iam_role" "fargate-ecs-role" {
  name               = "fargate-ecs-role"
   assume_role_policy = jsonencode(
      {
        "Version": "2012-10-17",
        "Statement": [
            {
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "ecs-tasks.amazonaws.com"
            },
            "Effect": "Allow"
            }
        ]
      }
    ) 
}

#create a policy to allow logging 
resource "aws_iam_policy" "fargate_task_execution" {
  name = "fargate_task_execution"
  path = "/"
  description = "Allow  ecs task executions"
  policy = jsonencode(
     {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ecr:GetAuthorizationToken",
                "ecr:BatchCheckLayerAvailability",
                "ecr:GetDownloadUrlForLayer",
                "ecr:BatchGetImage",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "*"
        }
    ]
}
  )
}

#create a policy to allow autoscaling fargate
resource "aws_iam_policy" "fargate_scaling" {
  name = "fargate_scaling"
  path = "/"
  description = "Allow  autoscaling"
  policy = jsonencode(
      {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ecs:DescribeServices",
                "ecs:UpdateService",
                "cloudwatch:PutMetricAlarm",
                "cloudwatch:DescribeAlarms",
                "cloudwatch:DeleteAlarms"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
  )
}

#create a policy to loadbalancer
resource "aws_iam_policy" "fargate_loadbalancer" {
  name = "fargate_loadbalancer"
  path = "/"
  description = "loadbalancer"
  policy = jsonencode(
     {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ec2:DescribeAddresses",
                "ec2:DescribeCoipPools",
                "ec2:DescribeInstances",
                "ec2:DescribeNetworkInterfaces",
                "ec2:DescribeSubnets",
                "ec2:DescribeSecurityGroups",
                "ec2:DescribeVpcs",
                "ec2:DescribeInternetGateways",
                "ec2:DescribeAccountAttributes",
                "ec2:DescribeClassicLinkInstances",
                "ec2:DescribeVpcClassicLink",
                "ec2:CreateSecurityGroup",
                "ec2:CreateNetworkInterface",
                "ec2:DeleteNetworkInterface",
                "ec2:GetCoipPoolUsage",
                "ec2:ModifyNetworkInterfaceAttribute",
                "ec2:AllocateAddress",
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:AssociateAddress",
                "ec2:DisassociateAddress",
                "ec2:AttachNetworkInterface",
                "ec2:DetachNetworkInterface",
                "ec2:AssignPrivateIpAddresses",
                "ec2:AssignIpv6Addresses",
                "ec2:ReleaseAddress",
                "ec2:UnassignIpv6Addresses",
                "logs:CreateLogDelivery",
                "logs:GetLogDelivery",
                "logs:UpdateLogDelivery",
                "logs:DeleteLogDelivery",
                "logs:ListLogDeliveries",
                "outposts:GetOutpostInstanceTypes"
            ],
            "Resource": "*"
        }
    ]
}
  )
}

#attach fargate scaling policy to fargate-ecs role
resource "aws_iam_role_policy_attachment" "fargate_scaling" {
  role = aws_iam_role.fargate-ecs-role.name
  policy_arn = aws_iam_policy.fargate_scaling.arn
}

#attach fargate task execution policy to fargate-ecs role
resource "aws_iam_role_policy_attachment" "fargate_task_execution" {
  role = aws_iam_role.fargate-ecs-role.name
  policy_arn = aws_iam_policy.fargate_task_execution.arn
}

#attach loadbalancer policy to fargate-ecs role
resource "aws_iam_role_policy_attachment" "fargate_loadbalancer" {
  role = aws_iam_role.fargate-ecs-role.name
  policy_arn = aws_iam_policy.fargate_loadbalancer.arn
}