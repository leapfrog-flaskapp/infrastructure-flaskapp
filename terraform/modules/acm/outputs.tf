output "clientvpn_server_certificate" {
    value = aws_acm_certificate.flaskapp_vpn_server_cert
}

output "clientvpn_client_certificate" {
    value = aws_acm_certificate.flaskapp_vpn_client_cert
}