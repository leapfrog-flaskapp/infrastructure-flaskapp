data "aws_kms_secrets" "vpn_server_cert_ca" {
  secret {
    name    = "vpn_server_cert_ca"
    payload = filebase64("${path.module}/vpn_certificate/ca.encrypted")
  }
}

data "aws_kms_secrets" "vpn_server_cert_key" {
  secret {
    name    = "vpn_server_cert_key"
    payload = filebase64("${path.module}/vpn_certificate/server-key.encrypted")
  }
}

locals {
  vpn_server_certificate_chain = data.aws_kms_secrets.vpn_server_cert_ca.plaintext["vpn_server_cert_ca"]
}

locals {
  vpn_server_private_key = data.aws_kms_secrets.vpn_server_cert_key.plaintext["vpn_server_cert_key"]
}


#client certificates
data "aws_kms_secrets" "vpn_client_cert_ca" {
  secret {
    name    = "vpn_client_cert_ca"
    payload = filebase64("${path.module}/vpn_certificate/ca.encrypted")
  }
}

data "aws_kms_secrets" "vpn_client_cert_key" {
  secret {
    name    = "vpn_client_cert_key"
    payload = filebase64("${path.module}/vpn_certificate/client-key.encrypted")
  }
}

locals {
  vpn_client_certificate_chain = data.aws_kms_secrets.vpn_client_cert_ca.plaintext["vpn_client_cert_ca"]
}

locals {
  vpn_client_private_key = data.aws_kms_secrets.vpn_client_cert_key.plaintext["vpn_client_cert_key"]
}

resource "aws_acm_certificate" "flaskapp_vpn_client_cert" {
  certificate_body = file("${path.module}/vpn_certificate/client-certificate.crt")
  private_key      = local.vpn_client_private_key
  certificate_chain = local.vpn_client_certificate_chain
}

resource "aws_acm_certificate" "flaskapp_vpn_server_cert" {
  private_key      = local.vpn_server_private_key
  certificate_body = file("${path.module}/vpn_certificate/server-certificate.crt")
  certificate_chain = local.vpn_server_certificate_chain
}

# resource "aws_acm_certificate" "flaskapp_vpn_client_cert" {
#   certificate_body = file(var.client_certificate_path)
#   private_key      = file(var.client_key_path)
#   certificate_chain = file(var.ca_path)
# }

# resource "aws_acm_certificate" "flaskapp_vpn_server_cert" {
#   certificate_body = file(var.server_certificate_path)
#   private_key      = file(var.server_key_path)
#   certificate_chain = file(var.ca_path)
# }