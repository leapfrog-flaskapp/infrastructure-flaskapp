resource "aws_ec2_client_vpn_endpoint" "client_vpn" {
  description            = "flaskapp-db-vpn"
  server_certificate_arn = var.vpn_server_certificate.arn
  client_cidr_block      = "20.0.0.0/16"

  authentication_options {
    type                       = "certificate-authentication"
    root_certificate_chain_arn = var.vpn_client_certificate.arn
  }

  connection_log_options {
    enabled               = true
    cloudwatch_log_group  = var.loggroup.name
  }

  tags = {
    "Name" = "flaskapp-client-vpn"
  }
}


resource "aws_ec2_client_vpn_network_association" "client-vpn-db-subnet1" {
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.client_vpn.id
  subnet_id              = var.database_subnet_az1.id
  security_groups        = [var.vpn_securitygroup.id]
}

resource "aws_ec2_client_vpn_network_association" "client-vpn-db-subnet2" {
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.client_vpn.id
  subnet_id              = var.database_subnet_az2.id
  security_groups        = [var.vpn_securitygroup.id]
}

resource "aws_ec2_client_vpn_network_association" "client-vpn-db-subnet3" {
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.client_vpn.id
  subnet_id              = var.database_subnet_az3.id
  security_groups        = [var.vpn_securitygroup.id]
}

resource "aws_ec2_client_vpn_authorization_rule" "authorize-db-az1" {
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.client_vpn.id
  target_network_cidr    = var.database_subnet_az1.cidr_block
  authorize_all_groups   = true
}

resource "aws_ec2_client_vpn_authorization_rule" "authorize-db-az2" {
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.client_vpn.id
  target_network_cidr    = var.database_subnet_az2.cidr_block
  authorize_all_groups   = true
}

resource "aws_ec2_client_vpn_authorization_rule" "authorize-db-az3" {
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.client_vpn.id
  target_network_cidr    = var.database_subnet_az3.cidr_block
  authorize_all_groups   = true
}