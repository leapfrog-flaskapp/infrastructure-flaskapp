variable "scale_out_cooldown" {
  type = number
}

variable "scale_in_cooldown" {
  type = number
}

variable "average_memory_usage" {
  type = number
}

variable "scale_min_capacity" {
  type = number
}

variable "scale_max_capacity" {
  type = number
}

variable "fargate_cluster" {}
variable "fargate_cluster_service" {}