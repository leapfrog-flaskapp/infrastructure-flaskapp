resource "aws_appautoscaling_target" "flaskapp-fargate-autoscale" {
  max_capacity = var.scale_max_capacity
  min_capacity = var.scale_min_capacity
  resource_id = "service/${var.fargate_cluster.name}/${var.fargate_cluster_service.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace = "ecs"
}

resource "aws_appautoscaling_policy" "flaskapp-cpu-sca-usageling" {
  name = "flaskapp-cpu-sca-usageling"
  policy_type = "TargetTrackingScaling"
  resource_id = aws_appautoscaling_target.flaskapp-fargate-autoscale.resource_id
  scalable_dimension = aws_appautoscaling_target.flaskapp-fargate-autoscale.scalable_dimension
  service_namespace = aws_appautoscaling_target.flaskapp-fargate-autoscale.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageMemoryUtilization"
    }
    target_value = var.average_memory_usage
    scale_in_cooldown = var.scale_in_cooldown
    scale_out_cooldown = var.scale_out_cooldown
  }
}