data "aws_elb_service_account" "elb_account" {}
data "aws_caller_identity" "current" {}

#s3 bucket for elb accesslogs
resource "aws_s3_bucket" "flaskapp-elb-logs" {
  bucket_prefix = "${var.buketnameprefix}-elb-logs"
  
  force_destroy = true
}

#s3 bucket policy for elb accesslogs
resource "aws_s3_bucket_policy" "flaskapp-elb-log-plicy" {
  bucket = aws_s3_bucket.flaskapp-elb-logs.id
  policy = jsonencode({
    "Version": "2012-10-17",
    "Id": "AWSs3-ELB-AccessLogs-Policy",
    "Statement": [
        {
            "Sid": "AWSConsoleStmt-1624729686855",
            "Effect": "Allow",
            "Principal": {
                "AWS": "${data.aws_elb_service_account.elb_account.arn}"
            },
            "Action": "s3:PutObject",
            "Resource": "${aws_s3_bucket.flaskapp-elb-logs.arn}/AWSLogs/${data.aws_caller_identity.current.id}/*"
        },
        {
            "Sid": "AWSLogDeliveryWrite",
            "Effect": "Allow",
            "Principal": {
                "Service": "delivery.logs.amazonaws.com"
            },
            "Action": "s3:PutObject",
            "Resource": "${aws_s3_bucket.flaskapp-elb-logs.arn}/AWSLogs/${data.aws_caller_identity.current.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        },
        {
            "Sid": "AWSLogDeliveryAclCheck",
            "Effect": "Allow",
            "Principal": {
                "Service": "delivery.logs.amazonaws.com"
            },
            "Action": "s3:GetBucketAcl",
            "Resource": "${aws_s3_bucket.flaskapp-elb-logs.arn}"
        }
    ]
})
}