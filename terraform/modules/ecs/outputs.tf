output "fargate_cluster" {
  value = aws_ecs_cluster.flaskapp-cluster
}

output "fargate_cluster_service" {
  value = aws_ecs_service.flaskapp
}