#create a flaskapp cluster
resource "aws_ecs_cluster" "flaskapp-cluster" {
  name = "flaskapp"
  setting {
    name = "containerInsights"
    value = "enabled"
  }
}

#task definition for flaskapp container
resource "aws_ecs_task_definition" "flaskapp" {
  family = var.task_family
  requires_compatibilities = ["FARGATE"]
  network_mode = "awsvpc"
  execution_role_arn = var.fargate_ecs_role.arn
  task_role_arn = var.fargate_ecs_role.arn
  cpu = var.task_cpu
  memory = var.task_memory
  container_definitions = jsonencode([
    {
      name      = "${var.container_name}"
      image     = "${var.container_image}"
      essential = true
      portMappings = [
        {
          containerPort = 80
        }
      ]
    }
  ])
}

#create a flaskapp service for the cluster
resource "aws_ecs_service" "flaskapp" {
  name            = "flaskapp"
  cluster         = aws_ecs_cluster.flaskapp-cluster.id
  launch_type     = "FARGATE"
  task_definition = aws_ecs_task_definition.flaskapp.arn
  desired_count   = 1
 
 network_configuration {
   subnets = [var.fargate_subnet_az1.id,var.fargate_subnet_az2.id,var.fargate_subnet_az3.id]
   security_groups = [var.fargate_securitygroup.id]
   assign_public_ip = false
 }

 load_balancer {
    target_group_arn = var.fargate_targetgroup.arn 
    container_name   = var.container_name
    container_port   = 80
 }

 lifecycle {
    ignore_changes = [desired_count]
  }
}

