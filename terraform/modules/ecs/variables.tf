variable "fargate_ecs_role" {}
variable "fargate_subnet_az1"{}
variable "fargate_subnet_az2"{}
variable "fargate_subnet_az3"{}
variable "fargate_securitygroup"{}
variable "fargate_targetgroup"{}

variable "task_cpu" {
    type = number
}

variable "task_memory" {
  type = number
}

variable "container_image" {
  type = string
}

variable "container_name" {
  type = string
}

variable "task_family" {
  type = string
}

variable "cloudwatch_log_group" {}