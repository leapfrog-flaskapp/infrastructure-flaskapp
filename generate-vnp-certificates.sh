#!/bin/bash

#cleanup
cleanup(){
  rm -rf pki
  rm -rf easy-rsa
}

cms_key_id=$1

if [[ $cms_key_id == "" ]];then
   echo "no kms key id passed for encryption. Pass in the key id as argument"
   echo "command format = ./generate-vpn-certificates.sh aws_kms_key_id"
   exit 1
fi

current_timestamp=$(date +"%Y%m%d%H%M%S")
server_fqdn="$current_timestamp-server.flaskapp.cer"
client_fqdn="$current_timestamp-client.flaskapp.cer"

server_certificate_body="pki/issued/$server_fqdn.crt"
server_private_key="pki/private/$server_fqdn.key"
client_certificate_body="pki/issued/$client_fqdn.crt"
client_private_key="pki/private/$client_fqdn.key"
ca="pki/ca.crt"

rm -rf easy-rsa
rm -rf pki
rm -rf vpncertificates

git clone https://github.com/OpenVPN/easy-rsa.git 

easy-rsa/easyrsa3/easyrsa init-pki
easy-rsa/easyrsa3/easyrsa build-ca nopass
easy-rsa/easyrsa3/easyrsa build-server-full $server_fqdn nopass
easy-rsa/easyrsa3/easyrsa build-client-full $client_fqdn nopass

#encrypt server private key
if test -f "$server_private_key"; then
    echo "encrypting server certificate private key."
    aws kms encrypt \
      --key-id $cms_key_id \
      --plaintext fileb://$server_private_key \
      --output text \
      --query CiphertextBlob | base64 \
      --decode > /tmp/${server_fqdn}-key.tmp
      if [ -f /tmp/${server_fqdn}-key.tmp ]
      then
        if [ -s /tmp/${server_fqdn}-key.tmp  ]
        then
            echo "encrypting server certificate private key passed.."
        else
            echo "encryption failed. Check your key id"
            cleanup
            exit 1
        fi
       else
        echo "encryption failed."
        cleanup
        exit 1
      fi  
else
    echo -e "ERROR!! No certificate found in $server_private_key"
fi

#encrypt client private key
if test -f "$client_private_key"; then
    echo "encrypting client certificate private key."
    aws kms encrypt \
      --key-id $cms_key_id \
      --plaintext fileb://$client_private_key \
      --output text \
      --query CiphertextBlob | base64 \
      --decode > /tmp/${client_fqdn}-key.tmp
      if [ -f /tmp/${client_fqdn}-key.tmp ]
      then
        if [ -s /tmp/${client_fqdn}-key.tmp  ]
        then
            echo "encrypting client certificate private key passed.."
        else
            echo "encryption failed. Check your key id"
            cleanup
            exit 1
        fi
       else
        echo "encryption failed."
        cleanup
        exit 1
      fi  
else
    echo -e "ERROR!! No certificate found in $client_private_key"
fi

#encrypt ca chain 
if test -f "$ca"; then
    echo "encrypting certificate chain."
    aws kms encrypt \
      --key-id $cms_key_id \
      --plaintext fileb://$ca \
      --output text \
      --query CiphertextBlob | base64 \
      --decode > /tmp/ca.tmp
       if [ -f /tmp/ca.tmp ]
      then
        if [ -s /tmp/ca.tmp  ]
        then
            echo "encrypting ca passed.."
        else
            echo "encryption failed. Check your key id"
            cleanup
            exit 1
        fi
       else
        echo "encryption failed."
        cleanup
        exit 1
      fi  
else
    echo -e "ERROR!! No certificate found in $ca"
fi


#copy server certificate to acm module in terraform
cp $server_certificate_body ./terraform/modules/acm/vpn_certificate/server-certificate.crt

#copy client certificate to acm module in terraform
cp $client_certificate_body ./terraform/modules/acm/vpn_certificate/client-certificate.crt

#copy server certificate encrypted key to acm module in terraform
cp /tmp/${server_fqdn}-key.tmp  terraform/modules/acm/vpn_certificate/server-key.encrypted

#copy client certificate encrypted key to acm module in terraform
cp /tmp/${client_fqdn}-key.tmp  terraform/modules/acm/vpn_certificate/client-key.encrypted

cp /tmp/ca.tmp terraform/modules/acm/vpn_certificate/ca.encrypted

#copy client certificates to vpncert directory
mkdir vpncertificates
cp $server_certificate_body vpncertificates/server_certificate_body.crt
cp $server_private_key vpncertificates/server_key.key
cp $client_certificate_body vpncertificates/client_certificate_body.crt
cp $client_private_key vpncertificates/client_key.key
cp $ca vpncertificates/ca.crt


cleanup

echo -e "server certificates generated sucessfully.\n"
