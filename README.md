This project contains terraform code to host a flask app in fargate.

Before commiting or running the terraform code,

1. create a cms key in aws in the region where this infrastructure is to be deployed
2. create a db-creds.yml file in current directory and insert the desired  username and password as follows

    username: flaskapp
    password: appleCatdog8

3. update the encrypt-db-credentials.sh script with the cms key id you created in aws
4. run the ./encrypt-db-credentials.sh script

this script will place an encrypted version of db username and password in the terraform rds module 
and will be used when running the terraform script.